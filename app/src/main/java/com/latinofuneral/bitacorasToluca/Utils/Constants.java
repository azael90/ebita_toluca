package com.latinofuneral.bitacorasToluca.Utils;

    public class Constants {


        public interface ACTION {
            public static String STARTFOREGROUND_ACTION = "com.latinofuneral.bitacorasToluca.foregroundservice.action.startforeground";
            public static String STOPFOREGROUND_ACTION = "com.latinofuneral.bitacoras2020.foregroundservice.action.stopforeground";
        }

        public interface NOTIFICATION_ID {
            public static int FOREGROUND_SERVICE = 101;
        }
    }