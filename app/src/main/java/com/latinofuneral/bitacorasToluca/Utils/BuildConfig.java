package com.latinofuneral.bitacorasToluca.Utils;


@SuppressWarnings("FieldCanBeLocal")
public final class BuildConfig {

    private static Branches targetBranch = Branches.TOLUCA;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{TOLUCA}

    private BuildConfig(){}

    public static Branches getTargetBranch(){
        return targetBranch;
    }

}
