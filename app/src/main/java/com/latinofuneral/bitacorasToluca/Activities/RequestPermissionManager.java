package com.latinofuneral.bitacorasToluca.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.latinofuneral.bitacorasToluca.Database.Choferes;
import com.latinofuneral.bitacorasToluca.Database.DatabaseAssistant;
import com.latinofuneral.bitacorasToluca.Database.LoginZone;
import com.latinofuneral.bitacorasToluca.Database.Lugares;
import com.latinofuneral.bitacorasToluca.MainActivity;
import com.latinofuneral.bitacorasToluca.R;
import com.latinofuneral.bitacorasToluca.Utils.ApplicationResourcesProvider;
import com.latinofuneral.bitacorasToluca.Utils.ConstantsBitacoras;
import com.latinofuneral.bitacorasToluca.Utils.Preferences;
import com.latinofuneral.bitacorasToluca.Utils.VolleySingleton;
import com.karan.churi.PermissionManager.PermissionManager;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import soup.neumorphism.NeumorphButton;


public class RequestPermissionManager extends AppCompatActivity {
    private static final String TAG = "PERMISION_MANAGER";
    private PermissionManager permissionManager;
    NeumorphButton btPermisos;
    Dialog dialogoError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permission_manager);
        btPermisos = (NeumorphButton) findViewById(R.id.btPermisos);
        dialogoError = new Dialog(RequestPermissionManager.this);



        if(Build.VERSION.SDK_INT >= 33){
            PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_MEDIA_IMAGES};
            if(ContextCompat.checkSelfPermission(RequestPermissionManager.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                if (!checkPermission(RequestPermissionManager.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(RequestPermissionManager.this, PERMISSIONS, FINE_LOCATION_PERMISSION_CODE);
                }
            }
            else
            {
                if(ActivityCompat.shouldShowRequestPermissionRationale(RequestPermissionManager.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)){
                    ActivityCompat.requestPermissions(RequestPermissionManager.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                    showLocationBackgroundRequest();
                }else
                {
                    ActivityCompat.requestPermissions(RequestPermissionManager.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                    showLocationBackgroundRequest();
                }
            }
        }else
        {
            PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (!checkPermission(RequestPermissionManager.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(RequestPermissionManager.this, PERMISSIONS, FINE_LOCATION_PERMISSION_CODE);
            }
        }



        permissionManager = new PermissionManager(){};
        permissionManager.checkAndRequestPermissions(RequestPermissionManager.this);

        if(ApplicationResourcesProvider.checkInternetConnection()) {
            downloadChoferesAndAyudantes();
            downloadPlaces();
            requestPlaceAndGeofenceZoneToLogin();
        }
        else
            showErrorDialog("No hay conexión a internet");

        //checkPermission();


        btPermisos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ApplicationResourcesProvider.checkInternetConnection()) {

                    Dexter.withContext(getApplicationContext())
                            .withPermissions
                                    (PERMISSIONS).withListener(
                                    new MultiplePermissionsListener() {
                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                                            if (multiplePermissionsReport.areAllPermissionsGranted()) {






                                                if(Build.VERSION.SDK_INT >= 29){
                                                    if(ContextCompat.checkSelfPermission(RequestPermissionManager.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED){
                                                        Toast.makeText(RequestPermissionManager.this, "Permisos concedidos", Toast.LENGTH_LONG).show();
                                                        Preferences.setPreferencePermissionsBoolean(RequestPermissionManager.this, true, Preferences.PREFERENCE_REQUEST_PERMISSIONS_GRANTED);

                                                        boolean checkInAlDia = Preferences.getPreferenceCheckinCheckoutAssistant(RequestPermissionManager.this, Preferences.PREFERENCE_CHECKIN_CHECKOUT_ASSISTANT);
                                                        if (checkInAlDia) {
                                                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        } else {
                                                            //Era login antes
                                                            Intent intent = new Intent(getBaseContext(), Welcome.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    }
                                                    else {
                                                        ActivityCompat.requestPermissions(RequestPermissionManager.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                                                        Toast.makeText(RequestPermissionManager.this, "Activa los permisos de ubicación de segundo plano para continuar", Toast.LENGTH_LONG).show();

                                                        TextView btPermisosManuales = (TextView) findViewById(R.id.btPermisosManuales);
                                                        YoYo.with(Techniques.Bounce).duration(700).repeat(6).playOn(btPermisosManuales);



                                                    }
                                                }else
                                                {
                                                    if (!checkPermission(RequestPermissionManager.this, PERMISSIONS)) {
                                                        ActivityCompat.requestPermissions(RequestPermissionManager.this, PERMISSIONS, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                                                        Toast.makeText(RequestPermissionManager.this, "4", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else{
                                                        Toast.makeText(RequestPermissionManager.this, "Permisos concedidos", Toast.LENGTH_LONG).show();
                                                        Preferences.setPreferencePermissionsBoolean(RequestPermissionManager.this, true, Preferences.PREFERENCE_REQUEST_PERMISSIONS_GRANTED);

                                                        boolean checkInAlDia = Preferences.getPreferenceCheckinCheckoutAssistant(RequestPermissionManager.this, Preferences.PREFERENCE_CHECKIN_CHECKOUT_ASSISTANT);
                                                        if (checkInAlDia) {
                                                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        } else {
                                                            //Era login antes
                                                            Intent intent = new Intent(getBaseContext(), Welcome.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    }
                                                }











                                            } else {
                                                Log.d(TAG, "onPermissionsChecked() returned: " + multiplePermissionsReport.getDeniedPermissionResponses());
                                                Toast.makeText(RequestPermissionManager.this, "Necesitamos los permisos activados", Toast.LENGTH_LONG).show();

                                                TextView btPermisosManuales = (TextView) findViewById(R.id.btPermisosManuales);
                                                YoYo.with(Techniques.Bounce).duration(700).repeat(6).playOn(btPermisosManuales);
                                            }
                                        }

                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                            permissionToken.continuePermissionRequest();
                                        }
                                    }).check();
                }else
                    showErrorDialog("No hay conexión a internet");








            }
        });


        TextView btPermisosManuales = (TextView) findViewById(R.id.btPermisosManuales);
        btPermisosManuales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
    }

    public void showErrorDialog(final String codeError) {
        final NeumorphButton btNo, btSi;
        TextView tvCodeError;
        dialogoError.setContentView(R.layout.layout_error);
        dialogoError.setCancelable(false);
        btNo = (NeumorphButton) dialogoError.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogoError.findViewById(R.id.btSi);
        tvCodeError = (TextView) dialogoError.findViewById(R.id.tvCodeError);
        tvCodeError.setText(codeError);

        if (codeError.equals("No hay conexión a internet")){
            btSi.setVisibility(View.GONE);
            btNo.setText("Verificar mas tarde");
        }

        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogoError.dismiss();
            }
        });

        dialogoError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogoError.show();

    }

    private void downloadChoferesAndAyudantes()
    {
        JSONObject params = new JSONObject();
        try {
            params.put("usuario", DatabaseAssistant.getUserNameFromSesiones() );
            params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
            params.put("isProveedor", DatabaseAssistant.getIsProveedor());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_DRIVERS_URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                Choferes.deleteAll(Choferes.class);
                JSONArray jsonArrayChoferes = new JSONArray();
                try {

                    jsonArrayChoferes = response.getJSONArray("drivers");

                    for (int i = 0; i <= jsonArrayChoferes.length() - 1; i++) {
                        DatabaseAssistant.insertarChoferes(
                                "" + jsonArrayChoferes.getJSONObject(i).getString("name"),
                                "" + jsonArrayChoferes.getJSONObject(i).getString("status"),
                                "" + jsonArrayChoferes.getJSONObject(i).getString("id")
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
        };
        //postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
    }

    private void downloadPlaces()
    {
        JSONObject params = new JSONObject();
        try {
            params.put("usuario", DatabaseAssistant.getUserNameFromSesiones() );
            params.put("token_device", DatabaseAssistant.getTokenDeUsuario());
            params.put("isProveedor", DatabaseAssistant.getIsProveedor());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsBitacoras.WS_PLACES_URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                Lugares.deleteAll(Lugares.class);
                JSONArray jsonArrayPlaces = new JSONArray();
                try {
                    jsonArrayPlaces = response.getJSONArray("places");

                    for (int i = 0; i <= jsonArrayPlaces.length() - 1; i++) {

                        DatabaseAssistant.insertarLugares(
                                "" + jsonArrayPlaces.getJSONObject(i).getString("name"),
                                "" + jsonArrayPlaces.getJSONObject(i).getString("status"),
                                "" + jsonArrayPlaces.getJSONObject(i).getString("latitud"),
                                "" + jsonArrayPlaces.getJSONObject(i).getString("longitud"),
                                "" + jsonArrayPlaces.getJSONObject(i).getString("perimetro"),
                                "" + jsonArrayPlaces.getJSONObject(i).getString("id")
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
        };
        //postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
    }



    private void requestPlaceAndGeofenceZoneToLogin() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantsBitacoras.WS_CONFIGURATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoginZone.deleteAll(LoginZone.class);
                    JSONObject json = new JSONObject(response);
                    JSONArray jsonArrayConfig = new JSONArray();
                    jsonArrayConfig = json.getJSONArray("config");

                    for (int i = 0; i <= jsonArrayConfig.length() - 1; i++) {

                        try{
                            DatabaseAssistant.insertarLoginZone(
                                    jsonArrayConfig.getJSONObject(i).getString("start_session_blocked"),
                                    jsonArrayConfig.getJSONObject(i).getString("start_session_place"),
                                    jsonArrayConfig.getJSONObject(i).getString("start_session_lat"),
                                    jsonArrayConfig.getJSONObject(i).getString("start_session_lng"),
                                    jsonArrayConfig.getJSONObject(i).getString("end_session_place"),
                                    jsonArrayConfig.getJSONObject(i).getString("end_session_lat"),
                                    jsonArrayConfig.getJSONObject(i).getString("end_session_lng"),
                                    jsonArrayConfig.getJSONObject(i).getString("end_session_blocked")
                            );
                        }catch (Throwable e){
                            Log.e(TAG, "requestPlaceAndGeofenceZoneToLogin: onResponse: " + e.getMessage());
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(80000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    private static final int READ_PERMISSION_CODE = 102;
    private static final int FINE_LOCATION_PERMISSION_CODE = 103;
    private static final int COARSE_LOCATION_PERMISSION_CODE = 104 ;
    private String[] PERMISSIONS = {
    };

    public static boolean checkPermission(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



    private void showLocationBackgroundRequest(){

        final NeumorphButton btNo, btSi;

        Dialog  dialogRequestBackgroundLocation = new Dialog(RequestPermissionManager.this);
        dialogRequestBackgroundLocation.setContentView(R.layout.layout_request_background_location);
        dialogRequestBackgroundLocation.setCancelable(false);
        btNo = (NeumorphButton) dialogRequestBackgroundLocation.findViewById(R.id.btNo);
        btSi = (NeumorphButton) dialogRequestBackgroundLocation.findViewById(R.id.btSi);



        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(RequestPermissionManager.this);

                builder.setTitle("Advertencia");
                builder.setMessage("Si no aceptas o deshabilitas los permisos de ubicación en segundo plano no podrás continuar con la aplicación");
                builder.setCancelable(false);
                builder.setPositiveButton("Cancelar", (dialog1, which) -> {
                    dialog1.dismiss();
                    dialog1.cancel();
                });
                builder.setNegativeButton("No aceptar permiso", (dialog1, which) -> {
                    //cancelar permiso
                    onDestroy();
                    finishAffinity();
                    Toast.makeText(getApplicationContext(), "No puedes continuar con la aplicación", Toast.LENGTH_SHORT).show();
                });

                builder.show();



            }
        });

        btSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogRequestBackgroundLocation.dismiss();
                //Acepta los permisos de ubicación en segundo plano

                if(ApplicationResourcesProvider.checkInternetConnection()) {

                    Dexter.withContext(getApplicationContext())
                            .withPermissions
                                    (Manifest.permission.ACCESS_FINE_LOCATION,
                                            Manifest.permission.CAMERA,
                                            Manifest.permission.ACCESS_COARSE_LOCATION,
                                            //Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(
                                    new MultiplePermissionsListener() {
                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                                            if (multiplePermissionsReport.areAllPermissionsGranted()) {


                                                if(Build.VERSION.SDK_INT >= 29){
                                                    if(ContextCompat.checkSelfPermission(RequestPermissionManager.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                                                        if(ActivityCompat.shouldShowRequestPermissionRationale(RequestPermissionManager.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)){
                                                            ActivityCompat.requestPermissions(RequestPermissionManager.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                                                        }else {
                                                            ActivityCompat.requestPermissions(RequestPermissionManager.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                                                        }
                                                    }

                                                }

                                            } else {
                                                Log.d(TAG, "onPermissionsChecked() returned: " + multiplePermissionsReport.getDeniedPermissionResponses());
                                                Toast.makeText(RequestPermissionManager.this, "Necesitamos los permisos activados", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                            permissionToken.continuePermissionRequest();
                                        }
                                    }).check();
                }else
                    showErrorDialog("No hay conexión a internet");




            }
        });


        dialogRequestBackgroundLocation.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = dialogRequestBackgroundLocation.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogRequestBackgroundLocation.show();

    }

    private static final int BACKGROUND_LOCATION_ACCESS_REQUEST_CODE = 9999;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*if (requestCode == FINE_LOCATION_ACCESS_REQUEST_CODE) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        ///*check if user select "never ask again" when denying any permission
                       // if (!shouldShowRequestPermissionRationale(permissions[i])) {
                        //}


                }
            }
        }*/

        /*if (requestCode == BACKGROUND_LOCATION_ACCESS_REQUEST_CODE) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "PERMISOS", Toast.LENGTH_SHORT).show();
                }
            }
        }*/


        if (requestCode == BACKGROUND_LOCATION_ACCESS_REQUEST_CODE || requestCode == FINE_LOCATION_PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permisos otorgados", Toast.LENGTH_SHORT).show();
                TextView btPermisosManuales = (TextView) findViewById(R.id.btPermisosManuales);
                btPermisosManuales.setVisibility(View.GONE);
            } else {
                Toast.makeText(this, "Necesitas otorgar todos los permisos", Toast.LENGTH_LONG).show();
                TextView btPermisosManuales = (TextView) findViewById(R.id.btPermisosManuales);
                YoYo.with(Techniques.Bounce).duration(700).repeat(6).playOn(btPermisosManuales);

            }
        }

    }





   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE: STORAGE_PERMISSION_CODE: READ_PERMISSION_CODE: FINE_LOCATION_PERMISSION_CODE: COARSE_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }

    }

    private void checkPermission(){
        int permissionCameraCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int permissionWriteStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionReadStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionCameraCheck != PackageManager.PERMISSION_GRANTED
                && permissionWriteStorage != PackageManager.PERMISSION_GRANTED
                && permissionReadStorage != PackageManager.PERMISSION_GRANTED
                && permissionFineLocation != PackageManager.PERMISSION_GRANTED
                && permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            ) {
                showExplanation("Permission Needed", "Rationale", Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
            } else {
                requestPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
            }


        } else {
            Toast.makeText(this, "Permission (already) Granted!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION_CODE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_PERMISSION_CODE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, COARSE_LOCATION_PERMISSION_CODE);
    }*/
}