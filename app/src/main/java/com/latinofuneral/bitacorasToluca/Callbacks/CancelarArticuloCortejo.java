package com.latinofuneral.bitacorasToluca.Callbacks;

public interface CancelarArticuloCortejo {
    public void onClickCancelarArticuloCortejo(int position, String serie, String fecha, String bitacora);
}
