package com.latinofuneral.bitacorasToluca.Callbacks;

public interface TerminarBitacoraCallback {
    public void onClickTerminarBitacora(int position, String bitacora);
}
