package com.latinofuneral.bitacorasToluca.Callbacks;

public interface RegistrarSalidaCallback {
    public void onClickRegistrarSalida(int position, String bitacora, String destino);
}
