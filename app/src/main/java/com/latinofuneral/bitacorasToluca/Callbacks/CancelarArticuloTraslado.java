package com.latinofuneral.bitacorasToluca.Callbacks;

public interface CancelarArticuloTraslado {
    public void onClickCancelarArticuloTraslado(int position, String serie, String fecha, String bitacora);
}
