package com.latinofuneral.bitacorasToluca.Callbacks;

public interface EditarDestinoBitacora {
    public void onClickEditarDestinoBitacora(int position, String bitacora);
}
