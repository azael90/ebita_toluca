package com.latinofuneral.bitacorasToluca.Callbacks;

public interface CancelarArticuloRecoleccion {
    public void onClickCancelarArticuloRecoleccion(int position, String serie, String fecha, String bitacora);
}
