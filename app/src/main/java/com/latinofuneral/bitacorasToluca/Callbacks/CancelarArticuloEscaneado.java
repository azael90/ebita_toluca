package com.latinofuneral.bitacorasToluca.Callbacks;

public interface CancelarArticuloEscaneado {
    public void onClickCancelarArticulo(int position, String id);
}
