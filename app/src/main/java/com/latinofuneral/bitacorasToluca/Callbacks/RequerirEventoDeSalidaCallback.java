package com.latinofuneral.bitacorasToluca.Callbacks;

public interface RequerirEventoDeSalidaCallback {
    public void onRequeriedEventoSalida(int position, String bitacora);
}
