package com.latinofuneral.bitacorasToluca.Callbacks;

public interface CancelarArticuloInstalacion {
    public void onClickCancelarArticuloInstalacion(int position, String serie, String fecha, String bitacora);
}
