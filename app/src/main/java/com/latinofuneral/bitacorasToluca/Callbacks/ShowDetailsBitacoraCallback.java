package com.latinofuneral.bitacorasToluca.Callbacks;

public interface ShowDetailsBitacoraCallback {
    public void onClickShowDetails(int position, String bitacora);
}
